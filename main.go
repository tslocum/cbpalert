package main

import (
	"flag"
	"fmt"
	"log"
	"os/exec"
	"time"

	"github.com/martinlindhe/notify"
	cb "github.com/preichenberger/go-coinbasepro/v2"
	"github.com/shopspring/decimal"
)

var rateLimiter = time.NewTicker(time.Second / 2)

func main() {
	var (
		configCrypto    string
		configThreshold float64
	)
	flag.StringVar(&configCrypto, "crypto", "BTC-USD", "cryptocurrency")
	flag.Float64Var(&configThreshold, "threshold", 0, "price change threshold")
	flag.Parse()
	args := flag.Args()

	client := cb.NewClient()
	client.UpdateConfig(&cb.ClientConfig{
		BaseURL: "https://api.pro.coinbase.com",
	})

	var (
		initialPrice decimal.Decimal
		lowestPrice  decimal.Decimal
		highestPrice decimal.Decimal
	)

	threshold := decimal.NewFromFloat(configThreshold)

	var notified bool

	t := time.NewTicker(time.Second)
	for range t.C {
		<-rateLimiter.C

		book, err := client.GetBook(configCrypto, 1)
		if err != nil {
			println(err.Error())
		}

		lastPrice, err := decimal.NewFromString(book.Bids[0].Price)
		if err != nil {
			println(err.Error())
		}

		if initialPrice.IsZero() {
			initialPrice = lastPrice
		}

		if lowestPrice.IsZero() || lastPrice.LessThan(lowestPrice) {
			lowestPrice = lastPrice
		}

		if highestPrice.IsZero() || lastPrice.GreaterThan(highestPrice) {
			highestPrice = lastPrice
		}

		log.Printf("C: %s H: %s, L: %s", lastPrice, highestPrice, lowestPrice)

		if (threshold.GreaterThan(decimal.NewFromFloat(0)) && lastPrice.Sub(lowestPrice).GreaterThanOrEqual(threshold)) ||
			(threshold.LessThan(decimal.NewFromFloat(0)) && lastPrice.Sub(highestPrice).LessThanOrEqual(threshold)) {
			log.Println("REACHED THRESHOLD")
			if !notified {
				notify.Notify("cbpbot", configCrypto+" threshold reached", "Last price: "+lastPrice.String(), "")
				notified = true
			}
		}

		if notified {
			if len(args) == 0 {
				fmt.Print(string(byte(7)))
			} else {
				var cmdArgs []string
				if len(args) > 1 {
					cmdArgs = args[1:]
				}
				cmd := exec.Command(args[0], cmdArgs...)
				cmd.Run()
			}
		}
	}
}
