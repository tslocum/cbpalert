module gitlab.com/tslocum/cbpalert

go 1.15

require (
	github.com/deckarep/gosx-notifier v0.0.0-20180201035817-e127226297fb // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/martinlindhe/notify v0.0.0-20181008203735-20632c9a275a
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d // indirect
	github.com/preichenberger/go-coinbasepro/v2 v2.0.5
	github.com/shopspring/decimal v1.2.0
	gopkg.in/toast.v1 v1.0.0-20180812000517-0a84660828b2 // indirect
)
