# cbpalert
[![CI status](https://gitlab.com/tslocum/cbpalert/badges/master/pipeline.svg)](https://gitlab.com/tslocum/cbpalert/commits/master)
[![Donate](https://img.shields.io/liberapay/receives/rocketnine.space.svg?logo=liberapay)](https://liberapay.com/rocketnine.space)

Coinbase Pro cryptocurrency ticker with price alerts

## Compile

cbpalert is written in [Go](https://golang.org). Run the following command to
download and build cbpalert from source.

```bash
go get gitlab.com/tslocum/cbpalert
```

The resulting binary is available as `~/go/bin/cbpalert`.

## Usage

Alert when the price of `BTC-USD` increases by $100:

```bash
cbpalert --threshold=100
```

Alert when the price of `ETH-BTC` decreases by $200:

```bash
cbpalert --crypto=ETH-BTC --threshold=-200
```

Alert when the price of `ETH-BTC` decreases by $50 by playing a sound:

```bash
cbpalert --crypto=ETH-BTC --threshold=-200 mpg123 ~/Sounds/notification.mp3
```

## Support

Please share issues and suggestions [here](https://gitlab.com/tslocum/cbpalert/issues).
